{-# LANGUAGE DeriveGeneric #-}

module Level (
  StatsByLevel(..),
  loadStatsByLevel
) where

import Data.Aeson
import GHC.Generics

data StatsByLevel = StatsByLevel {
  hp :: [Int],
  ac :: [Int],
  dpr :: [Int],
  atk :: [Int]
} deriving (Generic, Show)

instance FromJSON StatsByLevel

loadStatsByLevel :: IO (Maybe StatsByLevel)
loadStatsByLevel = decodeFileStrict "database/stats-by-level.json"

