module Character (
  CharacterSpec(..),
  Character(..),
  newCharacter,
  dealDamage,
  pretty
) where

data CharacterSpec = CharacterSpec {
  max_hp :: Int,
  ac :: Int,
  atk_bonus :: Int,
  dpr :: Int
} deriving (Show)

data Character = Character {
  spec :: CharacterSpec,
  name :: String,
  damage :: Int
} deriving (Show)

newCharacter :: CharacterSpec -> String -> Character
newCharacter spec name = Character { spec = spec, name = name, damage = 0 }

dealDamage :: Character -> Int -> Character
dealDamage char amount = char { damage = damage char + amount }

stat :: Character -> (CharacterSpec -> a) -> a
stat char f = f (spec char)

hp :: Character -> Int
hp char = stat char max_hp - damage char

pretty :: Character -> String
pretty char = name char ++ ": " ++ show (hp char) ++ "/" ++ show (stat char max_hp)

