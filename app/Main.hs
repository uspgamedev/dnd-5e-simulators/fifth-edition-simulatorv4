module Main where

import Character
import Level(loadStatsByLevel)

main :: IO ()
main = do
  let spec = CharacterSpec {
    max_hp = 12, ac = 18, atk_bonus = 5, dpr = 9
  }
  let char = newCharacter spec "Omar"
  print (pretty char)
  let char2 = char `dealDamage` 3
  print (pretty char2)
  loaded <- loadStatsByLevel
  print loaded

